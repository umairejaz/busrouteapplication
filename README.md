#Bus Route Challenge

###Problem
We are adding a new bus provider to our system. In order to implement a very specific requirement of this bus provider our system needs to be able to filter direct connections. We have access to a weekly updated list of bus routes in form of a bus route data file. As this provider has a lot of long bus routes, we need to come up with a proper service to quickly answer if two given stations are connected by a bus route.

###Task
The bus route data file provided by the bus provider contains a list of bus routes. These routes consist of an unique identifier and a list of stations (also just unique identifiers). A bus route connects its list of stations.
Your task is to implement a micro service which is able to answer whether there is a bus route providing a direct connection between two given stations. Note: The station identifiers given in a query may not be part of any bus route!


#Bus Route Challenge Implementation

### Assumptions
* The Application will not start if datafile not provided.
* Stations are connected in the both direction(bidirectional).


### Testing
* Unit tests and Integration tests will be run during the build/package process.
* If the test fails, the application won't be packaged.

````
Umairs-MBP:tests umair$ sh build_docker_image.sh ~/java/busroutetest.zip 
zip mode
Sending build context to Docker daemon 18.02 MB
Step 1/13 : FROM ubuntu:16.04
 ---> 0ef2e08ed3fa
Step 2/13 : RUN apt-get update && apt-get install -y     openjdk-8-jdk     git     maven     gradle     unzip  && rm -rf /var/lib/apt/lists/*
 ---> Using cache
 ---> b080d30a7dd7
Step 3/13 : COPY example /data/example
 ---> Using cache
 ---> a41e3012f900
.
.
.
.
Step 13/13 : CMD bash /scripts/run.sh
 ---> Running in 6d7407737f50
 ---> abbffa406650
Removing intermediate container 6d7407737f50
Successfully built abbffa406650
Umairs-MBP:tests umair$ sh run_test_docker.sh 
TEST PASSED!


Umairs-MBP:tests umair$ sh run_test_docker.sh 
TEST PASSED!
Umairs-MBP:tests umair$ sh run_test_local.sh 
+++ dirname run_test_local.sh
++ cd .
++ pwd
+ DIR=/Users/umair/Projects/challenges/bus_route_challenge/tests
+ pushd /Users/umair/Projects/challenges/bus_route_challenge/tests
+ bash start /Users/umair/Projects/challenges/bus_route_challenge/tests/docker/example
bash: start: No such file or directory
+ bash simple_test.sh
TEST PASSED!

````