package goeuro.message;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;
/**
 *
 * @author Umair Ejaz <umairejaz.ch@gmail.com>
 */
@Value
public class DirectBusRouteResponse {

    @JsonProperty("dep_sid")
    private final Integer depSId;

    @JsonProperty("arr_sid")
    private final Integer arrSId;

    @JsonProperty("direct_bus_route")
    private final Boolean directBusRoute;
}
