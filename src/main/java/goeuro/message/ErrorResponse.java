package goeuro.message;

import lombok.Value;

import javax.ws.rs.core.Response;

/**
 *
 * @author Umair Ejaz <umairejaz.ch@gmail.com>
 */
@Value
public class ErrorResponse {
    private final Response.Status status;
    private final String message;

    public Response toResponse() {
        return Response.status(status).entity(this).build();
    }
}
