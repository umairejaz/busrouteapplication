package goeuro.service.implementation;

import jersey.repackaged.com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import goeuro.rest.BusRouteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Umair Ejaz <umairejaz.ch@gmail.com>
 */
@Slf4j
public class DirectRouteServiceImpl implements BusRouteService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DirectRouteServiceImpl.class.getName());
    private static final Set<Integer> EMPTY_LIST = Sets.newHashSet();
    private final Map<Integer, Set<Integer>> directRoutes;

    public DirectRouteServiceImpl(Map<Integer, Set<Integer>> directRoutes) {
        this.directRoutes = directRoutes;
    }

    @Override
    public boolean hasDirectConnection(int depStationId, int arrStationId) {
        LOGGER.debug("has DirectConnection from {} to {}", depStationId, arrStationId);
        return directRoutes.getOrDefault(depStationId, EMPTY_LIST).contains(arrStationId);
    }
}
