package goeuro.rest;

/**
 * @author Umair Ejaz <umairejaz.ch@gmail.com>
 */
public interface BusRouteService {

  /**
   * Check of direct route from departure station ID to arrival station ID.
   *
   * @param departureStationId the id of the departure station
   * @param arrivalStationId   the id of the arrival station
   * @return true if direct connection exists, false otherwise
   */
  boolean hasDirectConnection(int departureStationId, int arrivalStationId);

}
