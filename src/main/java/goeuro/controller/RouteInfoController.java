package goeuro.controller;

import goeuro.message.DirectBusRouteResponse;
import goeuro.message.ErrorResponse;
import goeuro.rest.BusRouteService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

/**
 * API controller
 *
 * @author Umair Ejaz <umairejaz.ch@gmail.com>
 */
@Provider
@Path("/api")
@Produces(MediaType.APPLICATION_JSON)
@Slf4j
public class RouteInfoController {

  private static final Logger LOGGER = LoggerFactory.getLogger(RouteInfoController.class.getName());

  private BusRouteService directRouteService;

  @Autowired
  public RouteInfoController(BusRouteService directRouteService) {
    this.directRouteService = directRouteService;
  }

  @GET
  @Path(value = "/direct")
  public DirectBusRouteResponse hasDirectConnection(@NotNull @QueryParam("dep_sid") String depSID,
                                                    @NotNull @QueryParam("arr_sid") String arrSID) {
    Integer departureStationId = parseInt(depSID, "dep_sid");
    Integer arrivalStationId = parseInt(arrSID, "arr_sid");
    boolean hasDirectRoute = directRouteService.hasDirectConnection(departureStationId, arrivalStationId);
    LOGGER.info("api/direct requested from {} to {}, respond with {}", departureStationId, arrivalStationId, hasDirectRoute);
    return new DirectBusRouteResponse(departureStationId, arrivalStationId, hasDirectRoute);
  }

  private Integer parseInt(String inputString, String fieldName) {
    try {
      return Integer.parseInt(inputString);
    } catch (NumberFormatException e) {
      String msg = String.format("Could not parse value for parameter \"%1s\" with value \"%2s\" to Integer. ", fieldName, inputString);
      ErrorResponse errorResponse = new ErrorResponse(Response.Status.BAD_REQUEST, msg);
      throw new WebApplicationException(errorResponse.toResponse());
    }
  }

}
