package goeuro.utils;

import jersey.repackaged.com.google.common.collect.Lists;
import jersey.repackaged.com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
/**
 * @author Umair Ejaz <umairejaz.ch@gmail.com>
 */

/**
 * RouteUtil provides functionality to process and extract route information.
 */
@Slf4j
public class RouteUtil {

  /**
   * Extracts direct connections from a list of route information strings in format according to goeuro task description.
   *
   * @param routes the list of route information, one route per list entry
   * @return a map containing all stations mapping to a list of their directly connected stations
   * @throws IOException in case an Exception occurs while reading the file
   */
  public static Map<Integer, Set<Integer>> extractDirectConnections(List<String> routes) throws IOException {
    Map<Integer, Set<Integer>> connections = Maps.newHashMap();
    routes.parallelStream().map(e -> e.split(" ")).map(RouteUtil::convertAndCollectIntRouteInfo)
        .forEach(stations -> fillConnectionMapFromRoute(stations, connections));
    return connections;
  }

  /**
   * Convert each element given in the route information line into Integer and collect it into a list
   *
   * @param routeElements Array of elements present in the route information line
   * @return List of Integers parsed from route information line
   */
  private static List<Integer> convertAndCollectIntRouteInfo(String[] routeElements) {
    List<Integer> convertedIDs = Lists.newArrayListWithCapacity(routeElements.length - 1);
    for (int i = 1; i < routeElements.length; i++) {
      convertedIDs.add(Integer.valueOf(routeElements[i]));
    }
    return convertedIDs;
  }

  /**
   * Fills the map of connections by adding all stations of this route to every station of this route.
   *
   * @param routeStationIds list of route station id's
   * @param connections     map that must be filled by adding all stations
   */
  private static void fillConnectionMapFromRoute(List<Integer> routeStationIds, Map<Integer, Set<Integer>> connections) {
    for (Integer stationId : routeStationIds) {
      Set<Integer> connectionsForStation = connections.computeIfAbsent(stationId, k -> new HashSet<>());
      connectionsForStation.addAll(routeStationIds);
    }
  }
}