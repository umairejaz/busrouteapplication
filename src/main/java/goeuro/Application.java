/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package goeuro;

import goeuro.controller.RouteInfoController;
import goeuro.rest.BusRouteService;
import goeuro.service.implementation.DirectRouteServiceImpl;
import goeuro.utils.RouteUtil;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Umair Ejaz <umairejaz.ch@gmail.com>
 */
@SpringBootApplication
public class Application {

  private static final Logger LOGGER = LoggerFactory.getLogger(Application.class.getName());

  public static void main(String[] args) {

    LOGGER.info("Loading bus route application with '{}' args.", args.length);

    SpringApplication.run(Application.class, args);
  }

  /**
   * @throws IOException
   */
  @Bean
  public BusRouteService directRouteService(@Value("${routes.filename}") String routesFilename) throws IOException {
    // Read file and create list of routes
    List<String> routes = Files.readAllLines(Paths.get(routesFilename));

    routes = routes.subList(1, routes.size());
    Map<Integer, Set<Integer>> directRoutesMap = RouteUtil.extractDirectConnections(routes);
    return new DirectRouteServiceImpl(directRoutesMap);
  }

  //configuration class
  @Configuration
  public class Config extends ResourceConfig {
    public Config() {
      register(RouteInfoController.class);
      property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);
    }
  }
}