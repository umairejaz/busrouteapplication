package goeuro.service.implementation;

import goeuro.service.implementation.DirectRouteServiceImpl;
import jersey.repackaged.com.google.common.collect.Maps;
import jersey.repackaged.com.google.common.collect.Sets;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runner.RunWith;
import org.junit.Test;
import goeuro.rest.BusRouteService;

import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(BlockJUnit4ClassRunner.class)
public class DirectRouteServiceTests {

  /**
   * Given direct connections when query has existing direct connection must return true.
   */
  @Test
  public void ExistentDirectConnectionQueryTest() {
    Map<Integer, Set<Integer>> connections = Maps.newHashMap();
    connections.put(1, Sets.newHashSet(2, 3, 4, 5));
    BusRouteService routeService = new DirectRouteServiceImpl(connections);
    boolean result = routeService.hasDirectConnection(1, 2);
    assertTrue("When direct connection exist - 'true' must be returned.", result);
  }

  /**
   * Given direct connections when query has nonexistent direct connection must return false.
   */
  @Test
  public void NonexistentDirectConnectionQueryTest() {
    Map<Integer, Set<Integer>> connections = Maps.newHashMap();
    connections.put(1, Sets.newHashSet(7, 1, 3, 5));
    BusRouteService routeService = new DirectRouteServiceImpl(connections);
    boolean result = routeService.hasDirectConnection(7, 5);
    assertFalse("When direct connection doesn't exist - false must be returned", result);
  }

  /**
   * Given direct connections when query has nonexistent station - 'false' must be returned.
   */
  @Test
  public void NonexistentStationQueryTest() {
    Map<Integer, Set<Integer>> connections = Maps.newHashMap();
    connections.put(1, Sets.newHashSet(9, 5, 1, 3));
    BusRouteService routeService = new DirectRouteServiceImpl(connections);
    boolean result = routeService.hasDirectConnection(12, 4);
    assertFalse("When direct connection doesn't exist - 'false' must be returned", result);
  }

}
