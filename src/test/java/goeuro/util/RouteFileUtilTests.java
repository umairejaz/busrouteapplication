package goeuro.util;

import jersey.repackaged.com.google.common.collect.Lists;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runner.RunWith;
import org.junit.Test;
import goeuro.utils.RouteUtil;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertTrue;


@RunWith(BlockJUnit4ClassRunner.class)
public class RouteFileUtilTests {

  /**
   * Given route from '0' station to '3' station and when we extract direct connections the
   * excepted result should be next: Station 0 must contain station 3 in the connection set.
   *
   * @throws IOException
   */
  @Test
  public void GivenRouteFrom0To3Test() throws IOException {
    List<String> routes = Lists.newArrayList();
    routes.add("3 0 1 4 3");
    Map<Integer, Set<Integer>> connections = RouteUtil.extractDirectConnections(routes);
    assertTrue("Excepted result: Station 0 must contain station 3 in the connection set.", connections.get(0).contains(3));
  }

  /**
   * Assuming that routes are bidirectional.
   * Given route from '0' station to '3' station and when we extract direct connections the
   * excepted result should be next: Station 3 must contain station 0 in connection set.
   *
   * @throws IOException when reading from file was failed.
   */
  @Test
  public void BidirectionalRouteFrom0To3Test() throws IOException {
    List<String> routes = Lists.newArrayList();
    routes.add("3 0 1 4 3");
    Map<Integer, Set<Integer>> connections = RouteUtil.extractDirectConnections(routes);
    assertTrue("Excepted result: Station 3 must contain station 0 the connection set.", connections.get(3).contains(0));
  }

  /**
   * Given no route from '0' station to '3' station and when direct routes are extracted
   * Excepted result: Station 0 must not contain station 3 in his connection set.
   *
   * @throws IOException when reading from file was failed.
   */
  @Test
  public void NoGivenRouteFrom0To3Test() throws IOException {
    List<String> routes = Lists.newArrayList();
    routes.add("3 0 1 4");
    routes.add("1 3 2 5");
    routes.add("0 9 0 4");
    Map<Integer, Set<Integer>> connections = RouteUtil.extractDirectConnections(routes);
    assertTrue("Excepted result: Station 0 must not contain station 3 in the connection set.", !connections.get(0).contains(3));
  }

  /**
   * When no routes was given, after extraction of direct routes
   * excepted result is: Map of connections must be empty.
   * givenNoRoutes_whenExtractDirectRoutes_returnEmptyMap
   *
   * @throws IOException when reading from file was failed.
   */
  @Test
  public void NoGivenRoutesTest() throws IOException {
    List<String> routes = Lists.newArrayList();
    Map<Integer, Set<Integer>> connections = RouteUtil.extractDirectConnections(routes);  //no routes was given
    assertTrue("Excepted result: connection map must be empty.", connections.isEmpty());
  }
}
